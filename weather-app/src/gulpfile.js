'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass'); 
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var autoprefixer = require('gulp-autoprefixer');
// end sass //


// end sprites //

gulp.task('sass', function () {
  return gulp.src('./sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(gulp.dest('./css'))
    .pipe(rename("style.min.css"))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./css'));
});
    gulp.task('watch', function () {
        gulp.watch('./sass/**/*.scss', ['sass']);

});
 