import { Component, Input } from '@angular/core'; 
import { WeatherService } from './weather.service';
import { Provider } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { WeatherItem } from './weather-item'; 


@Component({
	selector: 'weather-search',
	template: `
		<section class="weather-search">
			<form #heroForm="ngForm">   
				<input #box (keyup)="onKey(box.value)" type="text" (keyup)="search(box.value)" required>
				<button (click)="onClickMe()">Add City</button>
				<span class="found">City found:</span> {{data.name}}
			</form>
		</section>
	`,
	providers: [WeatherService]
})
export class WeatherSearchComponent {
	data: any = {};
	values = '';
 	constructor(private _weatherService: WeatherService) {}
	onKey(value: string) {
		this.values = value;
	}

	onClickMe() { 
		this._weatherService.searchWeatherDate(this.values)  
		.subscribe(
		 	data => {
		 		const weatherItem = new WeatherItem(data.name, data.weather[0].description, data.main.temp, data.wind.speed, data.main.humidity);
		 		this._weatherService.addWeatherItem(weatherItem); 
		 	}
	 	);
	}

	search(cityName: string) {
		this._weatherService.searchWeatherDate(cityName).subscribe( 
		(data) => { 
			this.data = data; 
		},
		(err) => {
			this.data = "";
		});
	};
}

