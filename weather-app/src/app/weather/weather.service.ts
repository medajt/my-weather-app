import { Injectable } from '@angular/core';
import { WEATHER_ITEMS } from './weather.date';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpClientModule } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { WeatherItem } from './weather-item';


@Injectable()
export class WeatherService {

	constructor(private http: HttpClient) {}

	getWeatherItems() {
		return WEATHER_ITEMS;
	}

	addWeatherItem(weatherItem: WeatherItem) {
		WEATHER_ITEMS.push(weatherItem);
	}

	removeWeatherItem(weatherItem: WeatherItem) {
		let index = WEATHER_ITEMS.indexOf(weatherItem);
		WEATHER_ITEMS.splice(index,1);
	}

	searchWeatherDate(cityName: string): Observable<any> {
		return this.http.get('http://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&APPID=7242123909f90b5870dd221bfda9176e&units=metric')
			.pipe(
        		map(response => response)  
			)
	}
}