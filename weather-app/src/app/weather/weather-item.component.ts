import { Component, Input } from '@angular/core';
import { WeatherItem } from './weather-item';
import { NgIf } from '@angular/common';
import { WeatherService } from './weather.service';
import { DatePipe } from '@angular/common';
// @Input() weather-item: WeatherItem;


@Component({
	selector: 'weather-item',
	template: `
		<article class="weather-content"> 
			<div class="col-1">
				<h3>{{ weatherItem.cityName }}</h3>
				<span>{{today | date}}</span>
			</div>
			<div class="col-2">
				<h2>{{weatherItem.temperature}}°C</h2>
				<span class="info">{{weatherItem.description}}</span>
			</div>
			<div class="col-3">
				<span> Humidity: {{weatherItem.Humidity}}%</span>
				<span>Wind: {{weatherItem.Wind}}km/h</span>
			</div>						
			<div class="remove "(click)="removeItem(weatherItem)">
				<span></span>
				<span></span>
			</div>  
		</article>	 
	`
})

export class WeatherItemComponent {
	@Input('item') weatherItem: WeatherItem;

	constructor(private _weatherService: WeatherService) {}

	removeItem(weatherItem: WeatherItem) {
		this._weatherService.removeWeatherItem(weatherItem);
	} 	
  	today = Date.now();
}
