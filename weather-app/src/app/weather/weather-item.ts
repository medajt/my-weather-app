import { NgModule } from '@angular/core';

export class WeatherItem {
	constructor(public cityName: string, public description: string, public temperature: number, public Humidity: number, public Wind: number) {}
}